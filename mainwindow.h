#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QMessageBox>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QImage>
#include <QMouseEvent>
#include <QHash>
#include <QFileDialog>

#include "sizedialog.h"
#include "canvas.h"
#include "colorpicker.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();
	void wheelEvent(QWheelEvent *event);
	void closeEvent(QCloseEvent *event);

private slots:
	void newCanvas();
	void closeCanvas();
	void saveCanvas();
	void openCanvas();

private:
	void setMenuBarItems();
	void setToolBarItems();

	Ui::MainWindow *ui;
	QGraphicsScene *scene;
	Canvas *canvas;
	ColorPicker *colorPicker;
	QString filename;
	//QHash<QString,Tool*> tools;

};
#endif // MAINWINDOW_H

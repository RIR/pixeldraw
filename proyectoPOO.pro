QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = PixelDraw
INCLUDEPATH += .

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    canvas.cpp \
    colorpicker.cpp \
    cubeta.cpp \
    eraser.cpp \
    gotero.cpp \
    main.cpp \
    mainwindow.cpp \
    pencil.cpp \
    sizedialog.cpp \
    tool.cpp

HEADERS += \
    canvas.h \
    colorpicker.h \
    cubeta.h \
    eraser.h \
    gotero.h \
    mainwindow.h \
    pencil.h \
    sizedialog.h \
    tool.h

FORMS += \
    mainwindow.ui \
    sizedialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#include "cubeta.h"
#include "canvas.h"
#include "QDebug"

Cubeta::Cubeta(ColorPicker *color): color(color){}

void Cubeta::mousePressEvent(Canvas *c, QGraphicsSceneMouseEvent *event)
{
	QColor pcolor = c->getPixelColor((int)event->pos().x(),(int)event->pos().y());
	pintar(c,(int)event->pos().x(),(int)event->pos().y(), pcolor, color->getCurrentColor());
	c->update();
}


void Cubeta::pintar(Canvas *canvas,int x, int y, QColor c, const QColor &brocha)
{
	if(c == brocha) return;

	QStack<QPoint> stack;
	stack.push(QPoint(x,y));

	int width = canvas->getWidth(), height = canvas->getHeight();

	while(!stack.isEmpty()){
		QPoint pt = stack.pop();
		if(pt.x() < 0 || pt.x() >= width || pt.y() < 0 || pt.y() >= height) continue;
		if(canvas->getPixelColor(pt.x(),pt.y()) != c) continue;

		canvas->setPixelColor(pt.x(),pt.y(),brocha);
		for(int i = 0; i < 4; i++){
			int nx = pt.x()+movx[i], ny = pt.y()+movy[i];
			if((nx >= 0 && nx < width) && (ny >= 0 && ny < height)){
				if(canvas->getPixelColor(nx,ny) == c) stack.push(QPoint(nx,ny));
			}
		}
	}
}

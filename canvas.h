#ifndef CANVAS_H
#define CANVAS_H

#include <QGraphicsItem>
#include <QImage>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

#include "tool.h"

class Canvas : public QGraphicsItem
{

public:
	Canvas(int width, int height);
	Canvas(const QString &fileName, const char *format = nullptr);
	~Canvas();
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override;
	QRectF boundingRect() const override;

	void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
	void focusInEvent(QFocusEvent *event);
	void focusOutEvent(QFocusEvent *event);
	void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
	void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
	void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
	void inputMethodEvent(QInputMethodEvent *event);
	void keyPressEvent(QKeyEvent *event);
	void keyReleaseEvent(QKeyEvent *event);
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

	int getWidth() const;
	int getHeight() const;

	void setCurrentTool(Tool *value);
	void deleteCurrentTool();

	QColor getPixelColor(int x, int y) const;
	void setPixelColor(int x, int y, const QColor &color);

	bool saveCanvas(const QString &fileName, const char *format = nullptr);

	bool wasEdited() const;

private:
	QImage *image;
	int width, height;
	Tool *currentTool;
	bool edited;
};

#endif // CANVAS_H

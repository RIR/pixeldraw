#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "pencil.h"
#include "cubeta.h"
#include "gotero.h"
#include "eraser.h"

#define WINDOWTITLE tr("PixelDraw")

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow),
	  scene(nullptr),
	  canvas(nullptr)
{
	ui->setupUi(this);
	scene = new QGraphicsScene();
	ui->graphicsView->setScene(scene);

	setMenuBarItems();
	setToolBarItems();
}

MainWindow::~MainWindow()
{
	delete ui;
	if(scene){
		scene->clear();
		canvas = nullptr;
		delete scene;
	}

	if(colorPicker) delete colorPicker;

}

void MainWindow::setMenuBarItems(){
	QMenu *filemenu = ui->menubar->addMenu(tr("&Archivo"));

	QAction *nuevo = new QAction(tr("&Nuevo"),this);
	nuevo->setShortcut(QKeySequence::New);
	nuevo->setStatusTip(tr("Abre un nuevo dibujo"));
	connect(nuevo, &QAction::triggered, this, &MainWindow::newCanvas);
	filemenu->addAction(nuevo);

	QAction *abrir = new QAction(tr("&Abrir Dibujo"),this);
	abrir->setShortcut(QKeySequence::Open);
	abrir->setStatusTip(tr("Abre un dibujo"));
	connect(abrir, &QAction::triggered, this, &MainWindow::openCanvas);
	filemenu->addAction(abrir);

	QAction *guardar = new QAction(tr("&Guardar"),this);
	guardar->setShortcut(QKeySequence::Save);
	guardar->setStatusTip(tr("Guarda el dibujo"));
	connect(guardar, &QAction::triggered, this, &MainWindow::saveCanvas);
	filemenu->addAction(guardar);

	QAction *cerrar = new QAction(tr("&Cerrar Dibujo"),this);
	cerrar->setShortcut(QKeySequence::Close);
	cerrar->setStatusTip(tr("Cierra el dibujo"));
	connect(cerrar, &QAction::triggered, this, &MainWindow::closeCanvas);
	filemenu->addAction(cerrar);

}

void MainWindow::setToolBarItems(){
	colorPicker = new ColorPicker(this);
	ui->toolBar->addWidget(colorPicker);

	QAction *lapiz = new QAction(tr("Lapiz"),this);
	//tools["lapiz"] = new Pencil(colorpick);
	ui->toolBar->addAction(lapiz);
	connect(lapiz,&QAction::triggered,this,[lapiz,this](){
		//if(canvas) canvas->setCurrentTool(tools["lapiz"]);
		if(canvas){
			canvas->deleteCurrentTool();
			canvas->setCurrentTool(new Pencil(colorPicker));
		}
	});

	QAction *cubeta = new QAction(tr("Cubeta"),this);
	ui->toolBar->addAction(cubeta);
	connect(cubeta,&QAction::triggered,this,[cubeta,this](){
		if(canvas){
			canvas->deleteCurrentTool();
			canvas->setCurrentTool(new Cubeta(colorPicker));
		}
	});

	QAction *goma = new QAction(tr("Borrador"),this);
	ui->toolBar->addAction(goma);
	connect(goma,&QAction::triggered,this,[goma,this](){
		if(canvas){
			canvas->deleteCurrentTool();
			canvas->setCurrentTool(new Eraser());
		}
	});

	QAction *gotero = new QAction(tr("Gotero"),this);
	ui->toolBar->addAction(gotero);
	connect(gotero,&QAction::triggered,this,[gotero,this](){
		if(canvas){
			canvas->deleteCurrentTool();
			canvas->setCurrentTool(new Gotero(colorPicker));
		}
	});

}

void MainWindow::newCanvas(){
	closeCanvas();
	if(!canvas){
		SizeDialog sizeDialog;
		if(sizeDialog.exec()){
			canvas = new Canvas(sizeDialog.getWidth(),sizeDialog.getHeight());
			scene->addItem(canvas);
			ui->graphicsView->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
		}
	}


}

void MainWindow::closeCanvas(){
	if(!canvas) return;
	if(canvas->wasEdited()){
		QMessageBox closemsg;
		closemsg.setWindowTitle("Cerrar dibujo");
		closemsg.setText("El dibujo ha sido modificado");
		closemsg.setInformativeText("Deseas guardar los cambios?");
		closemsg.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
		closemsg.setDefaultButton(QMessageBox::Save);
		int res = closemsg.exec();

		switch(res){
			case QMessageBox::Save:
				saveCanvas();
				closeCanvas();
				break;
			case QMessageBox::Discard:
				scene->clear();
				//delete canvas;
				canvas = nullptr;
				setWindowTitle(WINDOWTITLE);
				filename.clear();
				break;
			case QMessageBox::Cancel:
				return;
				break;
			default:
				break;
		}
	}

	scene->clear();
	canvas = nullptr;
	setWindowTitle(WINDOWTITLE);
	filename.clear();
}

void MainWindow::saveCanvas()
{
	if(!canvas) return;
	if(canvas->wasEdited()){
		if(!filename.isEmpty()) canvas->saveCanvas(filename);
		else{
			filename = QFileDialog::getSaveFileName(this,tr("Guardar Dibujo"),"~/",tr("PNG image (*.png);;JPG image (*.jpg *.jpeg);;Windows Bitmap (*.bmp)"));
			if(filename.isEmpty()) return;
			if(canvas->saveCanvas(filename)){
				setWindowTitle(WINDOWTITLE+tr("\t-\t")+filename);
			}else{
				QMessageBox::critical(this,tr("Error al guardar"),tr("Se ha producido un erro al guardar el archivo"));
			}
		}
	}

}

void MainWindow::openCanvas()
{
	closeCanvas();
	if(!canvas){
		filename = QFileDialog::getOpenFileName(this,tr("Abrir Dibujo"),"~/",tr("Images (*.jpg *.jpeg *.bmp *.png)"));
		if(filename.isEmpty()) return;
		canvas = new Canvas(filename);
		setWindowTitle(WINDOWTITLE+tr("\t-\t")+filename);
		scene->addItem(canvas);
		ui->graphicsView->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);

	}
}

void MainWindow::wheelEvent(QWheelEvent *event){
	qreal factor;
	if(event->angleDelta().y() > 0) factor = 1.1;
	else factor = 0.9;

	ui->graphicsView->scale(factor,factor);

	ui->graphicsView->centerOn(ui->graphicsView->mapFromScene(event->position()));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	closeCanvas();
	if(!canvas) event->accept();
	else event->ignore();
}

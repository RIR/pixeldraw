#include "pencil.h"
#include "canvas.h"
#include <QDebug>

Pencil::Pencil(ColorPicker *color):color(color),pressed(false){}

void Pencil::mousePressEvent(Canvas *c, QGraphicsSceneMouseEvent *event)
{
	c->setPixelColor((int)event->pos().x(),(int)event->pos().y(),color->getCurrentColor());
	c->update();
}

void Pencil::mouseMoveEvent(Canvas *c, QGraphicsSceneMouseEvent *event)
{
	if(event->buttons() == Qt::LeftButton){
		c->setPixelColor((int)event->pos().x(),(int)event->pos().y(),color->getCurrentColor());
		c->update();
	}

}

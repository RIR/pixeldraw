#include "sizedialog.h"
#include "ui_sizedialog.h"

SizeDialog::SizeDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::SizeDialog)
{
	ui->setupUi(this);
}

SizeDialog::~SizeDialog()
{
	delete ui;
}

int SizeDialog::getWidth(){
	return ui->spinBox->value();
}

int SizeDialog::getHeight(){
	return ui->spinBox_2->value();
}

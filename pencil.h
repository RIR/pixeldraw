#ifndef PENCIL_H
#define PENCIL_H

#include "tool.h"
#include "colorpicker.h"

class Pencil : public Tool
{
public:
	Pencil(ColorPicker *color);
	void mousePressEvent(Canvas *c, QGraphicsSceneMouseEvent *event) override;
	void mouseMoveEvent(Canvas *c, QGraphicsSceneMouseEvent *event) override;

private:
	ColorPicker *color;
	bool pressed;
};

#endif // PENCIL_H

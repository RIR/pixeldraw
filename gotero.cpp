#include "gotero.h"
#include "canvas.h"

Gotero::Gotero(ColorPicker *color):color(color){}

void Gotero::mousePressEvent(Canvas *c, QGraphicsSceneMouseEvent *event)
{
	color->setCurrentColor(c->getPixelColor((int)event->pos().x(),(int)event->pos().y()));
	color->update();
}

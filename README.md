# Proyecto POO: PixelDraw
## Integrantes
- Esteban Naranjo
- Nicolas Ramirez
- Eduardo Tapia 
- Nicolás Tapia

## Archivos
* El archivo makefile
* Las clases Canvas, Tool, ColorPicker, Pencil , Eraser, Cubeta, Gotero, MainWindow, SizeDialog.
* La carpeta documentacion.
* Informe

## Descripción
PixelDraw es un programa en el cual el usuario tiene el poder de crear pixelart a traves de un canvas de tamaño a elección y herramientas de dibujo.

## Como compilar
(Se requiere tener instalado Qt6 y make, el programa ha sido a diseñado para correr en windows pero deberia tambien poder compilarse y usarse en linux)
1. Dirigirse al directorio del proyecto
2. Ejecutar el comando "make"
	* make QMAKE="path/to/qmake"
3. El programa compilado se encontrara en "compiled/release/PixelDraw"

## Ejecucción 
* Para ejecutar el programa dirigirse a compiled/release/ y ejecutar PixelDraw
* Para limpiar la carpeta de los archivos de compilación ejecutar el comando "make clean".


## Links de interés
[Documentación](https://rir.gitlab.io/pixeldraw/)

[Releases](https://gitlab.com/RIR/pixeldraw/-/releases)
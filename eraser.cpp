#include "eraser.h"
#include "canvas.h"

Eraser::Eraser(){}

void Eraser::mousePressEvent(Canvas *c, QGraphicsSceneMouseEvent *event)
{
	c->setPixelColor((int)event->pos().x(),(int)event->pos().y(),Qt::white);
	c->update();
}

void Eraser::mouseMoveEvent(Canvas *c, QGraphicsSceneMouseEvent *event)
{
	if(event->buttons() == Qt::LeftButton){
		c->setPixelColor((int)event->pos().x(),(int)event->pos().y(),Qt::white);
		c->update();
	}
}

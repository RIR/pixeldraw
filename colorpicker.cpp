#include "colorpicker.h"

ColorPicker::ColorPicker(QWidget *parent ): QAbstractButton(parent), currentColor(Qt::black){
	connect(this,&ColorPicker::clicked,this,&ColorPicker::colorPickerDialog);
}


void ColorPicker::paintEvent(QPaintEvent *e)
{
	Q_UNUSED(e);
	QPainter qp(this);
	qp.setBrush(QBrush(currentColor));
	qp.setPen(QPen(Qt::white,4));
	qp.drawRect(rect());
}

void ColorPicker::colorPickerDialog()
{
	currentColor = QColorDialog::getColor(currentColor,this,tr("Selecciona un color"),QColorDialog::ShowAlphaChannel);
}

QColor ColorPicker::getCurrentColor() const
{
	return currentColor;
}

void ColorPicker::setCurrentColor(const QColor &value)
{
	currentColor = value;
}

QSize ColorPicker::sizeHint() const
{
	return QSize(40,40);
}

#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <QAbstractButton>
#include <QColorDialog>
#include <QPainter>

class ColorPicker : public QAbstractButton
{
	Q_OBJECT
public:
	ColorPicker(QWidget *parent = nullptr);
	void paintEvent(QPaintEvent *e);
	void colorPickerDialog();
	QColor getCurrentColor() const;
	void setCurrentColor(const QColor &value);
	QSize sizeHint() const;

private:
	QColor currentColor;
};

#endif // COLORPICKER_H

#ifndef TOOL_H
#define TOOL_H

#include <QGraphicsSceneContextMenuEvent>
#include <QFocusEvent>
#include <QGraphicsSceneHoverEvent>
#include <QInputMethodEvent>
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>

class Canvas;

class Tool
{
public:
	virtual ~Tool();
	virtual void contextMenuEvent(Canvas *c,QGraphicsSceneContextMenuEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void focusInEvent(Canvas *c, QFocusEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void focusOutEvent(Canvas *c, QFocusEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void hoverEnterEvent(Canvas *c, QGraphicsSceneHoverEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void hoverMoveEvent(Canvas *c, QGraphicsSceneHoverEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void hoverLeaveEvent(Canvas *c, QGraphicsSceneHoverEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void inputMethodEvent(Canvas *c, QInputMethodEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void keyPressEvent(Canvas *c, QKeyEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void keyReleaseEvent(Canvas *c, QKeyEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void mousePressEvent(Canvas *c, QGraphicsSceneMouseEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void mouseMoveEvent(Canvas *c, QGraphicsSceneMouseEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void mouseReleaseEvent(Canvas *c, QGraphicsSceneMouseEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}
	virtual void mouseDoubleClickEvent(Canvas *c, QGraphicsSceneMouseEvent *event) {Q_UNUSED(c);Q_UNUSED(event);return;}

};

#endif // TOOL_H

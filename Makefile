APP := PixelDraw
CCFLAGS := -Wall -pedantic 
CC := g++
MKDIR := mkdir -p

QMAKE := qmake

$(APP):
	-mkdir compiled
	$(QMAKE) ./proyectoPOO.pro -o ./compiled
	make -C ./compiled/

clean:
	rm -r compiled/

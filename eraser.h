#ifndef ERASER_H
#define ERASER_H

#include "tool.h"

class Eraser : public Tool
{
public:
	Eraser();
	void mousePressEvent(Canvas *c, QGraphicsSceneMouseEvent *event) override;
	void mouseMoveEvent(Canvas *c, QGraphicsSceneMouseEvent *event) override;
};

#endif // ERASER_H

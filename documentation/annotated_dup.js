var annotated_dup =
[
    [ "Canvas", "class_canvas.html", "class_canvas" ],
    [ "ColorPicker", "class_color_picker.html", "class_color_picker" ],
    [ "Cubeta", "class_cubeta.html", "class_cubeta" ],
    [ "Eraser", "class_eraser.html", "class_eraser" ],
    [ "Gotero", "class_gotero.html", "class_gotero" ],
    [ "MainWindow", "class_main_window.html", "class_main_window" ],
    [ "Pencil", "class_pencil.html", "class_pencil" ],
    [ "SizeDialog", "class_size_dialog.html", "class_size_dialog" ],
    [ "Tool", "class_tool.html", "class_tool" ]
];
var class_color_picker =
[
    [ "ColorPicker", "class_color_picker.html#ad47f5aefd4047fe98f66e5d3a833126a", null ],
    [ "colorPickerDialog", "class_color_picker.html#a8a302e75809ec08cb9175b2239c503da", null ],
    [ "getCurrentColor", "class_color_picker.html#a228642f3660a532f0dae58ce73f96efe", null ],
    [ "paintEvent", "class_color_picker.html#afa0bbf41340688c5e55c6075b06eb4f5", null ],
    [ "setCurrentColor", "class_color_picker.html#a9151bc8dc17aaeeb5474d4d9851e6281", null ],
    [ "sizeHint", "class_color_picker.html#a9847633cd95da8275bd52a40da3f8200", null ]
];
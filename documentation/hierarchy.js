var hierarchy =
[
    [ "QAbstractButton", null, [
      [ "ColorPicker", "class_color_picker.html", null ]
    ] ],
    [ "QDialog", null, [
      [ "SizeDialog", "class_size_dialog.html", null ]
    ] ],
    [ "QGraphicsItem", null, [
      [ "Canvas", "class_canvas.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "Tool", "class_tool.html", [
      [ "Cubeta", "class_cubeta.html", null ],
      [ "Eraser", "class_eraser.html", null ],
      [ "Gotero", "class_gotero.html", null ],
      [ "Pencil", "class_pencil.html", null ]
    ] ]
];
#ifndef GOTERO_H
#define GOTERO_H

#include "tool.h"
#include "colorpicker.h"

class Gotero : public Tool
{
public:
	Gotero(ColorPicker *color);
	void mousePressEvent(Canvas *c, QGraphicsSceneMouseEvent *event) override;

private:
	ColorPicker *color;
};

#endif // GOTERO_H

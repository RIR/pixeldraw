#include "canvas.h"

Canvas::Canvas(int width, int height): image(nullptr), width(width), height(height), currentTool(nullptr), edited(true){
	image = new QImage(width,height,QImage::Format_ARGB32);
	image->fill(Qt::white);
}

Canvas::Canvas(const QString &fileName, const char *format): image(nullptr), currentTool(nullptr), edited(false)
{
	image = new QImage(fileName, format);
	height = image->height();
	width = image->width();
}

Canvas::~Canvas(){
	prepareGeometryChange();
	if(image) delete image;
	image = nullptr;
}

void Canvas::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
	Q_UNUSED(option);Q_UNUSED(widget);
	if(image) painter->drawImage(0,0,*image);
}

QRectF Canvas::boundingRect() const {
	return QRectF(0,0,width,height);
}

/*void Canvas::mousePressEvent(QGraphicsSceneMouseEvent *event){
	qInfo()<<event->pos()<<"\n";
	image->setPixelColor((int)event->pos().x(),(int)event->pos().y(),Qt::black);
	update();
}*/

int Canvas::getWidth() const
{
	return width;
}

int Canvas::getHeight() const
{
	return height;
}

void Canvas::setCurrentTool(Tool *value)
{
	currentTool = value;
}

void Canvas::deleteCurrentTool()
{
	if(currentTool) delete currentTool;
	currentTool = nullptr;
}


QColor Canvas::getPixelColor(int x, int y) const
{
	return image->pixelColor(x,y);
}

void Canvas::setPixelColor(int x, int y, const QColor &color)
{
	if(x < 0 || x >= width || y < 0 || y >= height) return;
	image->setPixelColor(x,y,color);
	edited = true;
}

bool Canvas::saveCanvas(const QString &fileName, const char *format)
{
	 if(image->save(fileName,format,100)){
		 edited = false;
		 return true;
	 }
	 return false;
}

bool Canvas::wasEdited() const
{
	return edited;
}







// Default events

void Canvas::contextMenuEvent(QGraphicsSceneContextMenuEvent *event) {if(currentTool)currentTool->contextMenuEvent(this,event);}
void Canvas::focusInEvent(QFocusEvent *event) {if(currentTool)currentTool->focusInEvent(this,event);}
void Canvas::focusOutEvent(QFocusEvent *event) {if(currentTool)currentTool->focusOutEvent(this,event);}
void Canvas::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {if(currentTool)currentTool->hoverEnterEvent(this,event);}
void Canvas::hoverMoveEvent(QGraphicsSceneHoverEvent *event) {if(currentTool)currentTool->hoverMoveEvent(this,event);}
void Canvas::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {if(currentTool)currentTool->hoverLeaveEvent(this,event);}
void Canvas::inputMethodEvent(QInputMethodEvent *event) {if(currentTool)currentTool->inputMethodEvent(this,event);}
void Canvas::keyPressEvent(QKeyEvent *event) {if(currentTool)currentTool->keyPressEvent(this,event);}
void Canvas::keyReleaseEvent(QKeyEvent *event) {if(currentTool)currentTool->keyReleaseEvent(this,event);}
void Canvas::mousePressEvent(QGraphicsSceneMouseEvent *event) {if(currentTool)currentTool->mousePressEvent(this,event);}
void Canvas::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {if(currentTool)currentTool->mouseMoveEvent(this,event);}
void Canvas::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {if(currentTool)currentTool->mouseReleaseEvent(this,event);}
void Canvas::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) {if(currentTool)currentTool->mouseDoubleClickEvent(this,event);}


#ifndef CUBETA_H
#define CUBETA_H

#include "tool.h"
#include "colorpicker.h"

#include <QStack>

class Cubeta : public Tool
{
public:
	Cubeta(ColorPicker *color);
	void mousePressEvent(Canvas *c, QGraphicsSceneMouseEvent *event) override;

private:
	void pintar(Canvas *canvas,int x, int y, QColor c, const QColor &brocha);

	ColorPicker *color;
	int movx[4] = {-1, 0, 1, 0};
	int movy[4] = {0, 1, 0, -1};
};

#endif // CUBETA_H
